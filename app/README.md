# wutucall-app

> wutucall something that...

[ ![Codeship Status for codell-concepts/wutucall](https://app.codeship.com/projects/1770c100-aabb-0135-1313-32b65ef89d88/status?branch=master)](https://app.codeship.com/projects/256593)

### Build Setup

``` bash
# install globals
npm i -g serve

# install dependencies
npm i

# serve with hot reload at localhost:8080
npm start OR npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

### Try locally

*Service workers are only registered in production build so...*

``` bash
# build
npm run build

#serve
cd dist
serve

# open in browser
```

### Try on your device

```bash
# build
npm run build

# serve
cd dist
serve

# use ngrok to be able to view the app on your device
ngrok http 5000

# on your device, browse to the forwarding url provided by ngrok
...
Session Status                online                                                                        
Version                       2.1.18                                                                        
Region                        United States (us)                                                            
Web Interface                 http://127.0.0.1:4040                                                         
Forwarding                    http://xxxxxxxx.ngrok.io -> localhost:5000
...  
                                  
```



class AppState {

  constructor(){
    this._init();
  }

  _init(){
    this._isNewSearch = true;
    this._namesFound = false;
    this._namesRejected = false;
  }

  thatIsNewSearch(){
    this._init();
    return this;
  }

  withFoundNames(){
    this._namesFound = true;
    this._namesRejected = false;
    this._isNewSearch = false;
    return this;
  }

  withNoFoundNames(){
    this._namesFound = false;
    this._namesRejected = false;
    this._isNewSearch = false;
    return this;
  }

  whereUserRejectedNames(){
    this._namesRejected = true;
    this._namesFound = true;
    this._isNewSearch = false;
    return this;
  }

  build() {
    const state = {
      isNewSearch: this._isNewSearch,
      namesFound: this._namesFound,
      namesRejected: this._namesRejected
    };
    this._init();
    return state;
  }

}

export default new AppState();

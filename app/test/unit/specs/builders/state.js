import appState from './app-state';

class State {

  constructor(){
    this._init();
  }

  _init(){
    this._appState = appState.build();
  }

  withAppState(appState){
    this._appState = appState;
    return this;
  }

  build() {
    const state = {
      app: this._appState
    };
    this._init();
    return state;
  }

}

export default new State();

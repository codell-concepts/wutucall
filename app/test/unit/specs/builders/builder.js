import appState from './app-state';
import state from './state';

export default {
  appState,
  state
}

import { parse, normalize } from '@/services/parser';


describe('parser', () => {

  describe('when parsing the value', () => {

    it('should return empty values if no value provided', () => {
      parse().length.should.equal(0);
    });

    it('should return empty values with empty value provided', () => {
      parse('').length.should.equal(0);
    });

    it('should return empty values with whitespace value provided', () => {
      parse('  ').length.should.equal(0);
    });

    it('should return values with value separated by commas', () => {
      parse('xxxx,yyyy, zzzz').length.should.equal(3);
    });

    it('should return values with value separated by spaces', () => {
      parse('xxxx yyyy zzzz').length.should.equal(3);
    });

    it('should return values with value separated by spaces and commas', () => {
      parse('xxxx, yyyy zzzz').length.should.equal(3);
    });

  });

  describe('when normalizing a value', () => {

    it('should return valid value', () => {
      normalize('builds').should.equal('builds');
    });

    it('should convert to lowercase', () => {
      normalize('AbCd').should.equal('abcd');
    });

    it('should trim whitespace', () => {
      normalize('   abcd   ').should.equal('abcd');
    });

    it('should not be a valid value if less than 4 characters', () => {
      expect(normalize('xxx')).to.be.null;
    });

    it('should not be a valid value if considered useless', () => {
      expect(normalize('this')).to.be.null;
      expect(normalize('then')).to.be.null;
      expect(normalize('that')).to.be.null;
      expect(normalize('other')).to.be.null;
    });

  });

});

import { stateToProps } from '@/containers/app-content.container';
import builder from '../builders/builder';

describe('app content container', () => {

  describe('when the app is in new search', () => {

    const appState = builder.appState.thatIsNewSearch().build();
    const state = builder.state.withAppState(appState).build();

    it('should ask wutucall question', () => {
      stateToProps.askWutucall(state).should.be.true;
    });

    it('should not attempt to show names found', () => {
      stateToProps.showNames(state).should.be.false;
    });

    it('should not ask for names', () => {
      stateToProps.askForIdeas(state).should.be.false;
    });

  });

  describe('when the app found names', () => {

    const appState = builder.appState.withFoundNames().build();
    const state = builder.state.withAppState(appState).build();

    it('should show names found', () => {
      stateToProps.showNames(state).should.be.true;
    });

    it('should not ask wutucall question', () => {
      stateToProps.askWutucall(state).should.be.false;
    });

    it('should not ask for names', () => {
      stateToProps.askForIdeas(state).should.be.false;
    });

  });

  describe('when the app did not found names', () => {

    const appState = builder.appState.withNoFoundNames().build();
    const state = builder.state.withAppState(appState).build();

    it('should ask for names', () => {
      stateToProps.askForIdeas(state).should.be.true;
    });

    it('should not ask wutucall question', () => {
      stateToProps.askWutucall(state).should.be.false;
    });

    it('should not attempt to show names found', () => {
      stateToProps.showNames(state).should.be.false;
    });

  });

});

import sut from '@/store/modules/app-state';
import types from '@/store/action-types';
import builder from '../../builders/builder';

describe('app state', () => {

  describe('when names are found', () => {

    const state = builder.appState.build();

    before(() => {
      sut.mutations[types.findSucceeded](state, ['xxx']);
    });

    it('should no longer be indicating a new search', () => {
      state.isNewSearch.should.be.false;
    });

    it('should indicate names were found', () => {
      state.namesFound.should.be.true;
    });

  });

  describe('when names are not found', () => {

    const state = builder.appState.build();

    before(() => {
      sut.mutations[types.findSucceeded](state, []);
    });

    it('should no longer be indicating a new search', () => {
      state.isNewSearch.should.be.false;
    });

    it('should indicate no names were found', () => {
      state.namesFound.should.be.false;
    });

  });

  describe('when the user rejected the found names', () => {

    const state = builder.appState.build();

    before(() => {
      sut.mutations[types.namesRejected](state);
    });

    it('should indicate names were rejected', () => {
      state.namesRejected.should.be.true;
    });

  });

  describe('when a new search has been requested', () => {

    const state = builder.appState.build();

    before(() => {
      sut.mutations[types.newSearch](state, []);
    });

    it('should be indicating a new search', () => {
      state.isNewSearch.should.be.true;
    });

    it('should indicate no names have been found', () => {
      state.namesFound.should.be.false;
    });

    it('should indicate no names were rejected', () => {
      state.namesRejected.should.be.false;
    });

  });

});

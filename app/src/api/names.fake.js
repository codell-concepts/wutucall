const store = {};

const prefetch = async () => {
  store.creates = ['factory', 'builder', 'creator'];
  store.builds = ['factory', 'builder', 'creator'];
};

const findInStore = (terms) => {
  const found = terms.length === 0 ? [] : terms.map(term => store[term] || []).reduce((a, b) => [...a, ...b]);
  return Array.from(new Set(found));
};

const find = terms => (
  new Promise((resolve) => { setTimeout(() => resolve(findInStore(terms)), 2000); })
);

const add = async () => {};

export default { prefetch, find, add };

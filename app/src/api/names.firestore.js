import db from '@/services/firestore';

const prefetch = async () => {
  try {
    const names = await db.names();
    await names.get();
  } catch (err) {
    console.warn(`failed to prefetch names: '${err}'`);
  }
};

const handle = (err, term) => {
  if (err.code === 'aborted') {
    console.info(`app offline, cache has doc for '${term}'`);
  } else {
    console.error(err);
  }
  return err;
};

const find = async (terms) => {
  if (!terms || terms.length === 0) return [];

  const collection = await db.names();
  const promises = terms.map(term => (collection.doc(term).get().catch(err => handle(err, term))));
  const docs = await Promise.all(promises);
  const found = docs.map(doc => (doc.exists ? doc.data().names : [])).reduce((a, b) => ([...a, ...b]), []);
  return Array.from(new Set(found));
};

const add = async (names, terms) => {
  const collection = await db.names();
  terms.forEach(async (term) => {
    const ref = collection.doc(term);
    const doc = await ref.get();
    if (doc.exists) {
      ref.update({ names: [...doc.data().names, ...names] });
    } else {
      ref.set({ names });
    }
  });
};

export default { prefetch, find, add };

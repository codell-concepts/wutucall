import firebase from 'firebase';
import 'firebase/firestore';

const config = {
  apiKey: 'AIzaSyDCxZPOtfmbbO-4tF1C4zzANTehgVl09F4',
  authDomain: 'wutucall-firestore.firebaseapp.com',
  databaseURL: 'https://wutucall-firestore.firebaseio.com',
  projectId: 'wutucall-firestore',
  storageBucket: 'wutucall-firestore.appspot.com',
  messagingSenderId: '1019286981578'
};

firebase.initializeApp(config);

let db = null;

const getStore = async () => {
  if (db === null) {
    try {
      await firebase.firestore().enablePersistence();
    } catch (err) {
      if (err.code === 'failed-precondition') {
        console.warn('Multiple tabs are open for wutucall.  Offline persistence can only be enabled in one tab.');
      } else if (err.code === 'unimplemented') {
        console.warn('Current browser does not support features required for offline persistence.');
      } else {
        console.error(`failed to enable offline persistence: ${err}`);
      }
    }
    db = firebase.firestore();
  }
  return db;
};

export default {
  names: async () => {
    const store = await getStore();
    return store.collection('names');
  }
};

const minlength = 4;

const useless = new Set(['then', 'that', 'this', 'other']);

export const normalize = (term) => {
  if (!term) return null;

  const normalized = term.trim().toLowerCase();
  // normalized = normalized.endsWith('s') ? normalized : `${normalized}s`;
  return normalized.length < minlength || useless.has(normalized) ? null : normalized;
};

export const parse = (value) => {
  const normalized = (value || '').trim();
  if (!normalized) return [];

  return value.split(',')
    .map(term => term.split(' '))
    .reduce((a, b) => ([...a, ...b]))
    .map(normalize)
    .filter(term => !!term);
};


export default Object.freeze({
  online: 'ONLINE',
  offline: 'OFFLINE',
  find: 'FIND_NAMES',
  findSucceeded: 'FIND_NAMES_SUCCEEDED',
  findFailed: 'FIND_NAMES_FAILED',
  addNames: 'ADD_NAMES',
  addNamesSucceeded: 'ADD_NAMES_SUCCEEDED',
  likeName: 'LIKE_NAME',
  rejectNames: 'REJECT_NAMES',
  namesRejected: 'NAMES_REJECTED',
  newSearch: 'NEW_SEARCH'
});

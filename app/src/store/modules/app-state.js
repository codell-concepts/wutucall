import types from '@/store/action-types';

const initial = {
  online: navigator.onLine,
  isNewSearch: true,
  namesFound: false,
  namesRejected: false
};

export const actions = {
  [types.online]({ commit }) {
    commit(types.online);
  },
  [types.offline]({ commit }) {
    commit(types.offline);
  },
  [types.likeName]({ commit }) {
    commit(types.newSearch);
  },
  [types.rejectNames]({ commit }) {
    commit(types.namesRejected);
  },
  [types.newSearch]({ commit }) {
    commit(types.newSearch);
  }
};

const mutations = {
  [types.online](state) {
    state.online = true;
  },
  [types.offline](state) {
    state.online = false;
  },
  [types.findSucceeded](state, found) {
    state.isNewSearch = false;
    state.namesFound = found.length > 0;
  },
  [types.namesRejected](state) {
    state.namesRejected = true;
  },
  [types.newSearch](state) {
    state.isNewSearch = true;
    state.namesFound = false;
    state.namesRejected = false;
  }
};

export default {
  state: initial,
  actions,
  mutations
};

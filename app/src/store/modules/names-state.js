import api from 'api';
import { parse } from '@/services/parser';
import types from '@/store/action-types';

const initial = {
  query: '',
  found: [],
  status: { busy: false, failed: false }
};

export const actions = {
  async [types.find]({ commit }, query) {
    commit(types.find, query);
    commit(types.findSucceeded, await api.find(parse(query)));
  },
  async [types.addNames]({ commit, state }, names) {
    commit(types.addNames);
    commit(types.addNamesSucceeded, await api.add(parse(names), parse(state.query)));
    commit(types.newSearch);
  }
};

const mutations = {
  [types.find](state, query) {
    state.query = query;
    state.status = { ...state.status, busy: true };
  },
  [types.findSucceeded](state, found) {
    state.found = found;
    state.status = { ...state.status, busy: false };
  },
  [types.addNames](state) {
    state.status = { ...state.status, busy: true };
  },
  [types.addNamesSucceeded](state) {
    state.status = { ...state.status, busy: false };
  },
  [types.newSearch](state) {
    state.found = [];
    state.query = '';
  },
};

export default {
  state: initial,
  actions,
  mutations
};

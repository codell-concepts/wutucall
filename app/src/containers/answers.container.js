import { connect } from 'vuex-connect';
import actions from '@/store/action-types';
import Answers from '@/components/answers';

export default connect({
  stateToProps: {
    names(state) {
      return state.names.found;
    },
    query(state) {
      return state.names.query;
    }
  },

  methodsToProps: {
    onLike({ dispatch }, name) {
      dispatch(actions.likeName, name);
    },
    onReject({ dispatch }) {
      dispatch(actions.rejectNames);
    }
  },
})(Answers);

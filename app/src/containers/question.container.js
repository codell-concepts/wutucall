import { connect } from 'vuex-connect';
import actions from '@/store/action-types';
import Question from '@/components/question';

export default connect({
  stateToProps: {
    initialQuery(state) {
      return state.names.query;
    },
    status(state) {
      return state.names.status;
    }
  },

  methodsToProps: {
    onFind: ({ dispatch }, query) => {
      if (query.trim()) {
        dispatch(actions.find, query);
      }
    }
  },
})(Question);

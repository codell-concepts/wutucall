import { connect } from 'vuex-connect';
import AppContent from '@/components/app-content';

export const stateToProps = {
  askWutucall(state) {
    return state.app.isNewSearch;
  },
  showNames(state) {
    return state.app.namesFound && !state.app.namesRejected;
  },
  askForIdeas(state) {
    return (!state.app.namesFound && !state.app.isNewSearch) || state.app.namesRejected;
  },
  online(state) {
    return state.app.online;
  }
};

export default connect({
  stateToProps
})(AppContent);


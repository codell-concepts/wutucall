import { connect } from 'vuex-connect';
import actions from '@/store/action-types';
import AnyIdeas from '@/components/any-ideas';

export default connect({
  stateToProps: {
    query(state) {
      return state.names.query;
    }
  },

  methodsToProps: {
    onNoIdeas({ dispatch }) {
      dispatch(actions.newSearch);
    },
    onAddNames({ dispatch }, names) {
      if (names.trim()) {
        dispatch(actions.addNames, names);
      }
    }
  },
})(AnyIdeas);

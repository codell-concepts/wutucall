import Vue from 'vue';
import Vuetify from 'vuetify';
import api from 'api';
import store from '@/store';
import actions from '@/store/action-types';
import App from '@/components/app';

import('../node_modules/vuetify/dist/vuetify.min.css');

api.prefetch();
Vue.config.productionTip = false;
Vue.use(Vuetify);

window.addEventListener('online', async () => { await store.dispatch(actions.online); });
window.addEventListener('offline', async () => { await store.dispatch(actions.offline); });

/* eslint-disable no-new */
new Vue({
  store,
  template: '<App/>',
  components: { App },
}).$mount('#app');

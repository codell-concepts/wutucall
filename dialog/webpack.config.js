'use strict';

const path = require('path');
var nodeExternals = require('webpack-node-externals');

module.exports = {
    entry: [
        'babel-polyfill',
        './src/index.js'
    ],
    output: {
        filename: './index.js',
        libraryTarget: 'this'
    },
    target: 'node',
    resolve: {
        extensions: ['.js']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: [
                    'babel-loader'
                ],
                exclude: /node_modules/
            }
        ]
    },
    externals: [nodeExternals()]
};
(function(e, a) { for(var i in a) e[i] = a[i]; }(this, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    log: function log(error) {
        var message = '';
        if (error instanceof Error) {
            message = error.message;
        } else {
            message = error;
        }

        console.log('Fatal Error: ' + message);
    }
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.db = undefined;

var _firebaseFunctions = __webpack_require__(3);

var functions = _interopRequireWildcard(_firebaseFunctions);

var _firebaseAdmin = __webpack_require__(9);

var admin = _interopRequireWildcard(_firebaseAdmin);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

admin.initializeApp(functions.config().firebase);

var db = exports.db = admin.firestore();

exports.default = {
    db: db
};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var buildLikeKey = exports.buildLikeKey = function buildLikeKey(description, name) {
    return description.toLowerCase() + "-" + name.replace(/\s+/g, "-").toLowerCase();
};

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("firebase-functions");

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(5);
module.exports = __webpack_require__(6);


/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("babel-polyfill");

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.wutucall = undefined;

var _firebaseFunctions = __webpack_require__(3);

var functions = _interopRequireWildcard(_firebaseFunctions);

var _dialogFlowApp = __webpack_require__(7);

var _dialogFlowApp2 = _interopRequireDefault(_dialogFlowApp);

var _logger = __webpack_require__(0);

var _logger2 = _interopRequireDefault(_logger);

var _search = __webpack_require__(8);

var _thumbsUp = __webpack_require__(10);

var _create = __webpack_require__(11);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var wutucall = exports.wutucall = functions.https.onRequest(function (req, res) {
	console.log('headers: ' + JSON.stringify(req.headers));
	console.log('body: ' + JSON.stringify(req.body));

	var app = new _dialogFlowApp2.default(req, res);
	var action = app.getAction();

	switch (action) {
		case 'wutucall.search':
			return (0, _search.search)(app);
		case 'wutucall.thumbs-up':
			return (0, _thumbsUp.thumbsUp)(app);
		case 'wutucall.create':
			return (0, _create.create)(app);
		default:
			_logger2.default.log('Unhandled Action: ' + action);
	}
});

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DialogFlowApp = function () {
    function DialogFlowApp(req, res) {
        _classCallCheck(this, DialogFlowApp);

        this._req = req;
        this._res = res;
    }

    _createClass(DialogFlowApp, [{
        key: 'isSlack',
        value: function isSlack() {
            return this._req.body.hasOwnProperty('originalRequest') && this._req.body.originalRequest.source === 'slack';
        }
    }, {
        key: 'getAction',
        value: function getAction() {
            return this._req.body.result.action;
        }
    }, {
        key: 'getParam',
        value: function getParam(name) {
            return this._req.body.result.parameters[name];
        }
    }, {
        key: 'getContextParam',
        value: function getContextParam(context, name) {
            var index = this._req.body.result.contexts.findIndex(function (x) {
                return x.name === context;
            });
            return this._req.body.result.contexts[index].parameters[name];
        }
    }, {
        key: 'tell',
        value: function tell(value) {
            var contextOut = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

            var data = this.isSlack() ? { "slack": { "text": value } } : {};

            this._res.setHeader('Content-Type', 'application/json');
            this._res.send(JSON.stringify({
                "speech": value, "displayText": value, "data": data, "contextOut": contextOut
            }));
        }
    }, {
        key: 'error',
        value: function error(_error) {
            var message = _error instanceof Error ? _error.message : _error;
            this._res.setHeader('Content-Type', 'application/json');
            this._res.send(JSON.stringify({ error: message }));
        }
    }]);

    return DialogFlowApp;
}();

exports.default = DialogFlowApp;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.search = undefined;

var search = exports.search = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(app) {
        var description, contextOut, response, doc, names;
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        description = app.getParam('description').toLowerCase();
                        contextOut = [];
                        response = 'Sorry i can\'t find anything! You can create your own by saying Create!';
                        _context.next = 6;
                        return _database.db.collection('names').doc(description).get();

                    case 6:
                        doc = _context.sent;

                        if (doc.exists) {
                            names = doc.data().names;


                            response = 'How about one of these?\n' + doc.data().names.join(', ') + '.\nWhich one do you like the best?';
                            contextOut = [{ name: "response-names", lifespan: 2, parameters: { names: names } }];
                        }

                        app.tell(response, contextOut);

                        _context.next = 14;
                        break;

                    case 11:
                        _context.prev = 11;
                        _context.t0 = _context['catch'](0);

                        _logger2.default.log(_context.t0);

                    case 14:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 11]]);
    }));

    return function search(_x) {
        return _ref.apply(this, arguments);
    };
}();

var _database = __webpack_require__(1);

var _keyBuilder = __webpack_require__(2);

var _logger = __webpack_require__(0);

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("firebase-admin");

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.thumbsUp = undefined;

var thumbsUp = exports.thumbsUp = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(app) {
        var description, names, number, name, likedName, key, docRef, doc, likes;
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        description = app.getContextParam('wutucall-search', 'description');
                        names = app.getContextParam('response-names', 'names');
                        number = app.getParam('number');
                        name = app.getParam('name');
                        likedName = getLikedName(names, name, number);
                        key = (0, _keyBuilder.buildLikeKey)(description, likedName);
                        _context.next = 9;
                        return _database.db.collection('likes').doc(key);

                    case 9:
                        docRef = _context.sent;
                        _context.next = 12;
                        return docRef.get();

                    case 12:
                        doc = _context.sent;

                        if (doc.exists) {
                            likes = doc.data().likes + 1;

                            docRef.update({ likes: likes });

                            app.tell('You and ' + likes + ' people have liked this class name!');
                        } else {
                            app.tell('Sorry there is no match, please try again');
                        }
                        _context.next = 19;
                        break;

                    case 16:
                        _context.prev = 16;
                        _context.t0 = _context['catch'](0);

                        _logger2.default.log(_context.t0);

                    case 19:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 16]]);
    }));

    return function thumbsUp(_x) {
        return _ref.apply(this, arguments);
    };
}();

var _database = __webpack_require__(1);

var _keyBuilder = __webpack_require__(2);

var _logger = __webpack_require__(0);

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var getLikedName = function getLikedName(names, name, number) {
    var index = name ? names.findIndex(function (x) {
        return x.toLowerCase() === name.toLowerCase();
    }) : number - 1;
    return names[index];
};

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.create = undefined;

var create = exports.create = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(app) {
        var context, description, names;
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        context = 'create';
                        description = app.getContextParam(context, 'description');
                        names = app.getContextParam(context, 'names');
                        _context.next = 6;
                        return _database.db.collection('names').doc(description).set({ names: names });

                    case 6:

                        names.forEach(function (name) {
                            var key = (0, _keyBuilder.buildLikeKey)(description, name);
                            _database.db.collection('likes').doc(key).set({ likes: 0 });
                        });

                        app.tell('Nice! I\'ve added the class name description!');

                        _context.next = 13;
                        break;

                    case 10:
                        _context.prev = 10;
                        _context.t0 = _context['catch'](0);

                        _logger2.default.log(_context.t0);

                    case 13:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 10]]);
    }));

    return function create(_x) {
        return _ref.apply(this, arguments);
    };
}();

var _database = __webpack_require__(1);

var _keyBuilder = __webpack_require__(2);

var _logger = __webpack_require__(0);

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

/***/ })
/******/ ])));
export default {
    log: error => {
        let message = '';
        if (error instanceof Error) {
            message = error.message;
        } else {
            message = error;
        }

        console.log('Fatal Error: ' + message)
    }
}
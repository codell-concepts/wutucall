import { db } from '../database';
import { buildLikeKey } from '../key-builder';
import Logger from '../logger';

export async function create(app) {
    try {
        const context = 'create';
        const description = app.getContextParam(context, 'description');
        const names = app.getContextParam(context, 'names');

        await db.collection('names').doc(description).set({ names });

        names.forEach((name) => {
            const key = buildLikeKey(description, name);
            db.collection('likes').doc(key).set({ likes: 0 });
        });

        app.tell('Nice! I\'ve added the class name description!');

    } catch (error) {
        Logger.log(error);
    }
}
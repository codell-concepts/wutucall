import { db } from '../database';
import { buildLikeKey } from '../key-builder';
import Logger from '../logger';

const getLikedName = (names, name, number) => {
    const index = name ? names.findIndex(x => x.toLowerCase() === name.toLowerCase()) : number - 1;
    return names[index];
}

export async function thumbsUp(app) {
    try {
        const description = app.getContextParam('wutucall-search', 'description');
        const names = app.getContextParam('response-names', 'names');
        const number = app.getParam('number');
        const name = app.getParam('name');

        const likedName = getLikedName(names, name, number);
        const key = buildLikeKey(description, likedName);

        const docRef = await db.collection('likes').doc(key);
        const doc = await docRef.get();
        if (doc.exists) {
            const likes = doc.data().likes + 1;
            docRef.update({ likes });

            app.tell(`You and ${likes} people have liked this class name!`);
        } else {
            app.tell('Sorry there is no match, please try again');
        }
    } catch (error) {
        Logger.log(error);
    }
}

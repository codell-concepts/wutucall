import { db } from '../database';
import { buildLikeKey } from '../key-builder';
import Logger from '../logger';

export async function search(app) {
    try {
        const description = app.getParam('description').toLowerCase();

        let contextOut = [];
        let response = 'Sorry i can\'t find anything! You can create your own by saying Create!';

        const doc = await db.collection('names').doc(description).get();
        if (doc.exists) {
            const names = doc.data().names;

            response = `How about one of these?\n${doc.data().names.join(', ')}.\nWhich one do you like the best?`;
            contextOut = [{ name: "response-names", lifespan: 2, parameters: { names } }]
        }

        app.tell(response, contextOut);

    } catch (error) {
        Logger.log(error);
    }
}
class DialogFlowApp {
    constructor(req, res) {
        this._req = req;
        this._res = res;
    }

    isSlack() {
        return this._req.body.hasOwnProperty('originalRequest') &&
            this._req.body.originalRequest.source === 'slack';
    }

    getAction() {
        return this._req.body.result.action;
    }

    getParam(name) {
        return this._req.body.result.parameters[name];
    }

    getContextParam(context, name) {
        const index = this._req.body.result.contexts.findIndex(x => x.name === context);
        return this._req.body.result.contexts[index].parameters[name];
    }

    tell(value, contextOut = []) {
        const data = this.isSlack() ? { "slack": { "text": value } } : {};
        
        this._res.setHeader('Content-Type', 'application/json');
        this._res.send(JSON.stringify({
            "speech": value, "displayText": value, "data": data, "contextOut": contextOut
        }));
    }

    error(error) {
        var message = error instanceof Error ? error.message : error;
        this._res.setHeader('Content-Type', 'application/json');
        this._res.send(JSON.stringify({error: message}));
    }
}

export default DialogFlowApp;
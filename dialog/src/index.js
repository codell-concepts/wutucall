import * as functions from 'firebase-functions';
import DialogFlowApp from './dialog-flow-app';
import Logger from './logger';
import { search } from './actions/search';
import { thumbsUp } from './actions/thumbs-up';
import { create } from './actions/create';

export const wutucall = functions.https.onRequest((req, res) => {
	console.log('headers: ' + JSON.stringify(req.headers));
	console.log('body: ' + JSON.stringify(req.body));

	const app = new DialogFlowApp(req, res);
	const action = app.getAction();

	switch (action) {
		case 'wutucall.search':
			return search(app);
		case 'wutucall.thumbs-up':
			return thumbsUp(app);
		case 'wutucall.create':
			return create(app);
		default:
			Logger.log(`Unhandled Action: ${action}`);
	}
});
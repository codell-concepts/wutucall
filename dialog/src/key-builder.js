export const buildLikeKey = (description, name) =>
    `${description.toLowerCase()}-${name.replace(/\s+/g, "-").toLowerCase()}`;